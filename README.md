INTRODUCTION
------------

This module integrates the 'Anime.js' library:
  - https://github.com/juliangarnier/anime

Anime.js (/ˈæn.ə.meɪ/) is a lightweight JavaScript animation library
with a simple, yet powerful API.

It works with CSS properties, SVG, DOM attributes and JavaScript Objects.


FEATURES
--------

'jQuery Anime' plugin is:

  - Lightweight

  - Easy to use

  - Powerful

  - Responsive

  - Customizable


REQUIREMENTS
------------

Download 'Anime.js' Library:
  - https://github.com/juliangarnier/anime/archive/master.zip


INSTALLATION
------------

1. Download 'Anime' module archive
   - https://www.drupal.org/project/anime

2. Extract and place it in the root modules directory i.e.
   /modules/anime or /modules/contrib/anime

3. Create a libraries directory in the root, if not already there i.e.
   /libraries

4. Create a 'anime' directory inside it i.e.
   /libraries/anime

5. Download 'Anime' library
   - https://github.com/juliangarnier/anime/archive/master.zip

6. Place it in the /libraries/anime directory i.e.
   /libraries/anime/lib/anime.min.js

7. Now, enable 'Anime' module.


USAGE
-----

Here is an Anime example:

anime({
  targets: 'div',
  translateX: 250,
  rotate: '1turn',
  backgroundColor: '#FFF',
  duration: 800
});

Check official Anime.js documentation for more information:
  - https://animejs.com/documentation/


How does it Work?
=================

1. Download Anime.js library, Follow INSTALLATION in above.

2. Enable "Anime" module, Follow INSTALLATION in above.

3. Add script to your theme/module ja file, Follow USAGE in above.

4. Enjoy that.


MAINTAINERS
-----------

Current maintainer:

 * Mahyar Sabeti - https://www.drupal.org/u/mahyarsbt

DEMO
-----------
https://animejs.com/
